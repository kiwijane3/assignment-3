//
//  ProgressBarView.swift
//  SeniorMonitor
//
//  Created by Jane Fraser on 4/10/19.
//  Copyright © 2019 Luoja Labs. All rights reserved.
//

import Foundation
import UIKit

class ProgressBarView: UIView {

	// The progress, from 0 to 1
	public var progress: Double = 1 {
		didSet {
			setNeedsDisplay();
		}
	}
	
	public var colour: UIColor = .systemBlue {
		didSet {
			setNeedsDisplay();
		}
	}
	
	public var lineWidth: CGFloat = 8 {
		didSet {
			setNeedsDisplay();
		}
	}
	
	public var inset: CGFloat = 8 {
		didSet {
			setNeedsDisplay();
		}
	}
	
	public override var intrinsicContentSize: CGSize {
		get {
			// We want this view to have an intrinsic height of the line width so it will be tall enough to display the progress bar, and no intrinsic width, as we want to size our width based on constraints.
			return CGSize(width: UIView.noIntrinsicMetric, height: lineWidth);
		}
	}
	
	public override init(frame: CGRect) {
		super.init(frame: frame);
		setNeedsDisplay();
	}
	
	public required init?(coder: NSCoder) {
		super.init(coder: coder);
		setNeedsDisplay();
	}
	
	public init() {
		super.init(frame: .zero);
		setNeedsDisplay()
	}
	
    override func draw(_ rect: CGRect) {
		// Calculate the center of this view in the y axis
		let centerY = self.bounds.size.height / 2
        // Drawing is similar to the progress warning view; The progress in inset from the view margins by the defined value, and the length of the bar reflects the progress.
		let start = CGPoint(x: inset, y: centerY);
		let barLength = (self.frame.size.width - (inset * 2)) * CGFloat(progress);
		let end = CGPoint(x: inset + barLength, y: centerY);
		// Create and define the path.
		let path = UIBezierPath();
		path.move(to: start);
		path.addLine(to: end);
		// Setup line width, ends, and stroke colour.
		path.lineWidth = lineWidth;
		path.lineCapStyle = .round;
		colour.setStroke();
		path.stroke();
    }

}
