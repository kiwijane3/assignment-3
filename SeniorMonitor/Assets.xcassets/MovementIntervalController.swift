//
//  MovementIntervalController.swift
//  SeniorMonitor
//
//  Created by Jane Fraser on 9/10/19.
//  Copyright © 2019 Luoja Labs. All rights reserved.
//

import UIKit
import ScrollableGraphView

class MovementIntervalController: UIViewController, ScrollableGraphViewDataSource {

	@IBOutlet weak var graph: ScrollableGraphView!
	
	@IBOutlet weak var dailyAverageLabel: UILabel!
	
	private var referenceLines: ReferenceLines?;
	
	override func viewDidLoad() {
        super.viewDidLoad()
		let linePlot = LinePlot(identifier: "main");
		graph.shouldAdaptRange = true;
		linePlot.shouldFill = false;
		linePlot.lineStyle = .straight
		graph.addPlot(plot: linePlot);
		referenceLines = ReferenceLines();
		referenceLines?.includeMinMax = true;
		referenceLines?.shouldAddLabelsToIntermediateReferenceLines = true;
		referenceLines?.referenceLineNumberOfDecimalPlaces = 0;
		graph.addReferenceLines(referenceLines: ReferenceLines());
		graph.shouldRangeAlwaysStartAtZero = true;
		graph.dataSource = self;
		NotificationCenter.default.removeObserver(self);
		NotificationCenter.default.addObserver(self, selector: #selector(load), name: .clientUpdate, object: nil);
		load();
    }
    
	@objc public func load() {
		graph.reload();
		dailyAverageLabel.text = Client.shared.dailyAverageInterval?.asMinutesAndSeconds ?? "Unknown";
	}
	
	public func numberOfPoints() -> Int {
		Client.shared.hourlyAverageIntervals.keys.count;
	}
	
	public func label(atIndex index: Int) -> String {
		return display(forHour: Client.shared.recordedHours[index]);
	}
	
	public func value(forPlot plot: Plot, atIndex index: Int) -> Double {
		let hour = Client.shared.recordedHours[index];
		return Client.shared.hourlyAverageIntervals[hour]!;
	}
	
	@IBAction func debugDisplay(_ sender: Any) {
		Client.shared.createDebugHourlyIntervals();
		load();
	}
	
}

public func display(forHour hour: Int) -> String {
	var hour = hour;
	var segment = "AM";
	if hour >= 12 {
		segment = "PM";
	}
	if hour > 12 {
		hour -= 12;
	}
	return "\(hour)\(segment)";
}
