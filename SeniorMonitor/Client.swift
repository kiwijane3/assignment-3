//
//  Client.swift
//  SeniorMonitor
//
//  Created by Jane Fraser on 2/10/19.
//  Copyright © 2019 Luoja Labs. All rights reserved.
//

import Foundation
import CocoaMQTT

import SwiftCSV

public class Client: CocoaMQTTDelegate {
	
	
	public static let shared: Client = Client();
	
	private let mqttClient: CocoaMQTT;
	
	// Indicates whether a message has been received since the flag was last reset. Used for tracking whether a message has been found during background refresh.
	private var receivedMessageFlag: Bool = false;
	
	public private(set) var lastMovementTime: Date = Date();
	
	public private(set) var lastMovementLocation: Location? = nil;
	
	public private(set) var dailyAverageInterval: Double?;
	
	public private(set) var hourlyAverageIntervals: [Int: TimeInterval] = [Int: TimeInterval]();
	
	public var recordedHours: [Int] {
		return hourlyAverageIntervals.keys.sorted();
	}
	
	public var maxHourlyInterval: Double {
		return hourlyAverageIntervals.values.max() ?? 0;
	}
	
	public var timeSinceLastMovement: TimeInterval {
		get {
			// Calculate the interval between the current date and the last movement, and returns it as seconds.
			return max(lastMovementTime.distance(to: Date()), 0.0);
		}
	}
	
	// Records whether a notification has been sent for this period of inactivity. Set to true when notification is sent, set to false when movement is detected.
	public var notificationFlag: Bool = false;
	
	// When displaying the activity pattern or average intervals, we want to only display the current day, because showing all time seemed kind of impractical and a day seemed intutive. This gives the date when the data was last reset. If we want to add data, and the reset date was before the day for that data, reset the data before adding.
	private var resetDate: Date = Date();
	
	public private(set) var movementFrequencies: [Location: Int] = [Location: Int]();
	
	// Returns the relative frequencies of movement in each location, given as a 0-1 value representing the percentage of movement events that were in each location today.
	public var relativeFrequencies: [Location: Double] {
		get {
			// Retrieve the total number of movement events by summing the values for occurrence.
			let totalEvents = totalMovementEvents();
			// Calculate the relative frequencies for each location and them to the output.
			var output = [Location: Double]();
			for key in movementFrequencies.keys {
				output[key] = Double(movementFrequencies[key] ?? 0) / Double(totalEvents);
			}
			return output;
		}
	}
	
	public private(set) var batteryStatuses: [Location: Double] = [Location: Double]();
	
	public init() {
		let clientID = "CocoaMQTT-\(String(ProcessInfo().processIdentifier))";
		mqttClient = CocoaMQTT(clientID: clientID, host: "barretts.ecs.vuw.ac.nz", port: 1883);
		// Default values, replace with appropriate variables
		mqttClient.username = "test";
		mqttClient.password = "public";
		mqttClient.keepAlive = 60;
		mqttClient.delegate = self;
		mqttClient.connect();
	}
	
	public func enterForeground() {
		// We may have disconnected while in background, so ensure we have an active connection.
		debugPrint("Entering foreground");
		if mqttClient.connState == .disconnected {
			mqttClient.connect();
		}
		// We should stay connected while we are in the foreground, so set autoReconnect to true.
		mqttClient.autoReconnect = true;
	}
	
	public func enterBackground() {
		debugPrint("Entering background");
		// We should only connect when we do background updates, so we autoReconnect should be set to false.
		mqttClient.autoReconnect = false;
	}
	
	public func backgroundFetch(completion: @escaping (Bool) -> Void ) {
		debugPrint("Performing background fetch");
		if mqttClient.connState == .disconnected {
			mqttClient.connect();
		}
		receivedMessageFlag = false;
		// Stay alive for four seconds to receive messages.
		DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute: {
			completion(self.receivedMessageFlag);
		})
	}
	
	
	public func sendNotificationIfNeeded() {
		if timeSinceLastMovement >= (5 * 60) && !notificationFlag {
			// Check if we have alert permission.
			UNUserNotificationCenter.current().getNotificationSettings { (settings) in
				if settings.authorizationStatus == .authorized, settings.alertSetting == .enabled {
					let content = UNMutableNotificationContent();
					content.title = "Senior Inactive!";
					content.body = "The senior has had no activity in the past 5 minutes. Please check on the senior. There may be a problem."
					// Deliver notification immediately.
					let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0, repeats: false);
					let uuidString = UUID().uuidString;
					let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger);
					UNUserNotificationCenter.current().add(request) { (error) in
						if error == nil {
							self.notificationFlag = true;
						}
					}
				}
			}
		}
	}
	
	public func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
		if ack == .accept {
			mqtt.subscribe("swen325/a3");
			debugPrint("Connection accepted");
		} else {
			debugPrint("Connection failed: \(ack.description)");
		}
	}
	
	public func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16) {
		// Make sure we can parse the message and retrieve a location, since we need to do those to respond to a message.
		debugPrint("Received message: \(message.string ?? "nil")");
		if let text = message.string {
			let data = text.split(separator: ",");
			if let location = Location.from(string: String(data[1])) {
				// If we can read a true motion status and retrieve a timestamp, then update for that movement event.
				if (readMotion(for: String(data[2])) ?? false) == true, let date = readDate(dateString: String(data[0])) {
					updateForMovement(in: location, at: date);
				}
				// If we can read the battery status, update it.
				if let batteryPercentage = Double(String(data[3])) {
					// Battery is given between 0 and 100, as a percentage, while we internally store as 0-1. This converts to the appropriate format.
					let batteryStatus = batteryPercentage / 100.0;
					updateBattery(forSensorInLocation: location, batteryStatus: batteryStatus);
				}
			}
			// Broadcast a notification so view controllers can react.
			NotificationCenter.default.post(name: .clientUpdate, object: nil);
		}
		debugPrint("last movement time: \(lastMovementTime)");
		debugPrint("last movement location: \(lastMovementLocation)");
		debugPrint("Location Frequencies: \(movementFrequencies)");
		debugPrint("daily average interval: \(dailyAverageInterval)");
		debugPrint("hourly average intervals: \(hourlyAverageIntervals)");
		debugPrint("Battery Statuses: \(batteryStatuses)");
	}
	
	private func updateForMovement(in location: Location, at time: Date) {
		// Reset the data if the old date is from yesterday of earlier.
		debugPrint("Updating for movement");
		if isPreviousDay(self.resetDate, from: time) {
			movementFrequencies = [Location: Int]();
			hourlyAverageIntervals = [Int: TimeInterval]();
			resetDate = time;
		}
		updateLastMovement(in: location, at: time);
		updateMovementFrequencies(forMovementIn: location, at: time)
	}
	
	private func updateLastMovement(in location: Location, at time: Date) {
		// Only update if this movement is more recent than the last movement, in case we receive messages out of order for some reason.
		if lastMovementTime < time {
			// Update the average movement intervals
			// Start with the daily interval.
			if let dailyAverageInterval = dailyAverageInterval {
				self.dailyAverageInterval = (dailyAverageInterval + timeSinceLastMovement) / 2;
			} else {
				dailyAverageInterval = timeSinceLastMovement;
			}
			// Update the hourly interval.
			let hour = Calendar.current.component(.hour, from: time);
			if let hourlyAverageInterval = hourlyAverageIntervals[hour] {
				hourlyAverageIntervals[hour] = (hourlyAverageInterval + timeSinceLastMovement) / 2;
			} else {
				hourlyAverageIntervals[hour] = timeSinceLastMovement;
			}
			lastMovementTime = time;
			lastMovementLocation = location;
			// Reset the notification flag so we can send a notification again.
			notificationFlag = false;
		}
	}
	
	private func updateMovementFrequencies(forMovementIn location: Location, at time: Date) {
		// Increment the frequency value.
		let currentFrequency = movementFrequencies[location] ?? 0;
		movementFrequencies[location] = currentFrequency + 1;
	}
	
	private func updateBattery(forSensorInLocation location: Location, batteryStatus: Double) {
		debugPrint("Updating battery statuses");
		batteryStatuses[location] = batteryStatus;
	}
	
	// The date is given in a strange, non-iso format. This converts it to an appropriate format and produces an iso8601 date.
	private func readDate(dateString: String) -> Date? {
		// Convert the space to a T and a Z on the end, to conform with iso.
		var string = dateString;
		if let spaceIndex = string.firstIndex(of: " ") {
			string.remove(at: spaceIndex);
			string.insert("T", at: spaceIndex);
			// The time is given in the sensor's local time, without specifying the timezone. This code assumes the timestamp is given in the local timezone, and appends the relevant utc offset in iso format.
			let formatter = ISO8601DateFormatter();
			
			if let date = formatter.date(from: string) {
				// timestamps are local time, but internal date representations use GMT, so apply the offset for today.
				return date - TimeInterval(TimeZone.autoupdatingCurrent.secondsFromGMT());
			} else {
				return Date();
			}
		}
		return nil;
	}
	
	private func readMotion(for value: String) -> Bool? {
		if value == "0" {
			return false;
		} else if value == "1" {
			return true;
		}
		return nil;
	}
	
	// Returns the total number of movement events that have been recorded today.
	public func totalMovementEvents() -> Int {
		return movementFrequencies.reduce(0) { (sum, entry) in
			return sum + entry.value;
		}
	}
	
	public func createDebugHourlyIntervals() {
		for hour in (0...23) {
			hourlyAverageIntervals[hour] = Double.random(in: 1...240);
		}
	}
	
	public func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
		
	}
	
	public func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topics: [String]) {
		debugPrint("Subscribed to topics: \(topics)");
	}
	
	public func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
		
	}
	
	public func mqttDidPing(_ mqtt: CocoaMQTT) {
		
	}
	
	public func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
		
	}
	
	public func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
		debugPrint("Disconnected. Error: \(err.debugDescription ?? "None")");
	}
	
	public func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
		
	}
	
}

public enum Location {
	case living, dining, kitchen, toilet, bedroom
}

public extension Location {
	
	// Converts a string to the respective location enum case, if one exists.
	public static func from(string: String) -> Location? {
		switch string {
		case "living":
			return .living;
		case "dining":
			return .dining;
		case "kitchen":
			return .kitchen;
		case "toilet":
			return .toilet;
		case "bedroom":
			return .bedroom;
		default:
			return nil;
		}
	}
	
	// Returns a human readable description of the case enum.
	public var description: String {
		get {
			switch self {
			case .living:
				return "Living Room";
			case .dining:
				return "Dining Room";
			case .kitchen:
				return "Kitchen";
			case .toilet:
				return "Bathroom";
			case .bedroom:
				return "Bedroom";
			}
		}
	}
	
}

// Returns whether the first date is on a day before the day the second date is on.
public func isPreviousDay(_ firstDate: Date, from secondDate: Date) -> Bool {
	let startOfFirstDay = Calendar.current.startOfDay(for: firstDate);
	let startOfSecondDay = Calendar.current.startOfDay(for: secondDate);
	return startOfFirstDay < startOfSecondDay;
}
