//
//  TimeInterval.swift
//  SeniorMonitor
//
//  Created by Jane Fraser on 4/10/19.
//  Copyright © 2019 Luoja Labs. All rights reserved.
//

import Foundation

public extension TimeInterval {
	
	public var asMinutesAndSeconds: String {
		get {
			let minuteComponent = (self / 60).rounded(.towardZero);
			let secondsComponent = self.truncatingRemainder(dividingBy: 60).rounded(.towardZero);
			let minutesFormatter = NumberFormatter();
			minutesFormatter.minimumIntegerDigits = 1;
			// Use a formatter so we always have two digits for the seconds component.
			let secondsFormatter = NumberFormatter();
			secondsFormatter.minimumIntegerDigits = 2;
			if let minutesString = minutesFormatter.string(from: NSNumber(value: minuteComponent)),
				let secondsString = secondsFormatter.string(from: NSNumber(value: secondsComponent)) {
				return "\(minutesString):\(secondsString)";
			}
			return "??:??";
		}
	}
	
}
