//
//  MovementController.swift
//  SeniorMonitor
//
//  Created by Jane Fraser on 4/10/19.
//  Copyright © 2019 Luoja Labs. All rights reserved.
//

import UIKit

class MovementController: UIViewController {

	@IBOutlet weak var movementAlertCountdownView: WarningCircularProgressView?;
	
	@IBOutlet weak var livingRoomFrequencyView: ProgressBarView?;
	
	@IBOutlet weak var kitchenFrequencyView: ProgressBarView?;
	
	@IBOutlet weak var diningRoomFrequencyView: ProgressBarView?;
	
	@IBOutlet weak var bathroomFrequencyView: ProgressBarView?;
	
	@IBOutlet weak var bedroomFrequencyView: ProgressBarView?;
	
	private var regularUpdateTimer: Timer? = nil;
	
	override func viewDidLoad() {
		super.viewDidLoad();
		NotificationCenter.default.removeObserver(self);
		NotificationCenter.default.addObserver(self, selector: #selector(load), name: .clientUpdate, object: nil);
		movementAlertCountdownView?.label?.numberOfLines = 2;
		movementAlertCountdownView?.label?.textAlignment = .center;
		livingRoomFrequencyView?.progress = 0.0;
		kitchenFrequencyView?.progress = 0.0;
		diningRoomFrequencyView?.progress = 0.0;
		bathroomFrequencyView?.progress = 0.0;
		bedroomFrequencyView?.progress = 0.0;
		load();
		// Setup a timer to reload frequently, so we can see the time progress.
		setupTimer()
    }
    
	private func frequencyView(for location: Location) -> ProgressBarView? {
		switch location {
		case .living:
			return livingRoomFrequencyView;
		case .kitchen:
			return kitchenFrequencyView;
		case .dining:
			return diningRoomFrequencyView;
		case .toilet:
			return bathroomFrequencyView
		case .bedroom:
			return bedroomFrequencyView;
		}
	}
	
	@objc private func load() {
		loadCountdown();
		loadFrequencies();
	}
	
	// Reloads the countdown view.
	@objc private func loadCountdown() {
		// Update the progress value.
		let timeSinceMovement = Client.shared.timeSinceLastMovement;
		let lastLocation = Client.shared.lastMovementLocation;
		// We want to alert after 5 minutes, so the progress should be 1 at that point; Thus, divide the time by five minutes to create this situation. Time intervals are in seconds.
		// This is just for testing, remove the max
		movementAlertCountdownView?.progress = timeSinceMovement / (5 * 60);
		// Create the Label for the progress view. We want two lines, one showing time since movement in minutes and seconds, and the other giving the last location.
		let text = "\(timeSinceMovement.asMinutesAndSeconds)\r\n\(lastLocation?.description ?? "Unknown")";
		movementAlertCountdownView?.text = text;
	}
	
	@objc private func loadFrequencies() {
		// Update the frequencies for each of the frequency displays.
		let relativeFrequencies = Client.shared.relativeFrequencies;
		debugPrint("\(relativeFrequencies)");
		for location in relativeFrequencies.keys {
			frequencyView(for: location)?.progress = relativeFrequencies[location] ?? 0.0;
		}
	}

	// Creates a timer to automatically update the progress timer.
	@objc public func setupTimer() {
		regularUpdateTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(loadCountdown), userInfo: nil, repeats: true);
	}
	
	@objc public func cancelTimer() {
		regularUpdateTimer?.invalidate();
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
