//
//  BatterStatusController.swift
//  SeniorMonitor
//
//  Created by Jane Fraser on 4/10/19.
//  Copyright © 2019 Luoja Labs. All rights reserved.
//

import UIKit

class BatteryStatusController: UIViewController {

	@IBOutlet weak var livingRoomIndicator: WarningLinearProgressView?
	
	@IBOutlet weak var kitchenIndicator: WarningLinearProgressView?
	
	@IBOutlet weak var diningRoomIndicator: WarningLinearProgressView?
	
	@IBOutlet weak var bathroomIndicator: WarningLinearProgressView?
	
	@IBOutlet weak var bedroomIndicator: WarningLinearProgressView?
	
	private var reloadTimer: Timer?;
	
	override func viewDidLoad() {
        super.viewDidLoad()
		// Setup the indicators so they will show the warning when their progress is equal to or less than 20%, which is a low battery status.
		livingRoomIndicator?.warningMode = .lessThan;
		livingRoomIndicator?.threshold = 0.2;
		kitchenIndicator?.warningMode = .lessThan;
		kitchenIndicator?.threshold = 0.2;
		diningRoomIndicator?.warningMode = .lessThan;
		diningRoomIndicator?.threshold = 0.2;
		bathroomIndicator?.warningMode = .lessThan;
		bathroomIndicator?.threshold = 0.2;
		bedroomIndicator?.warningMode = .lessThan;
		bedroomIndicator?.threshold = 0.2;
		// Register for notifications
		NotificationCenter.default.removeObserver(self);
		NotificationCenter.default.addObserver(self, selector: #selector(load), name: .clientUpdate, object: nil);
		load();
		view.setNeedsDisplay();
		reloadTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(load), userInfo: nil, repeats: true)
    }
    
	private func indicator(for location: Location) -> WarningLinearProgressView? {
		switch location {
		case .living:
			return livingRoomIndicator;
		case .kitchen:
			return kitchenIndicator;
		case .dining:
			return diningRoomIndicator;
		case .toilet:
			return bathroomIndicator;
		case .bedroom:
			return bedroomIndicator;
		}
	}
	
	@objc public func load() {
		for location in Client.shared.batteryStatuses.keys {
			indicator(for: location)?.progress = Client.shared.batteryStatuses[location] ?? 1;
		}
	}

}
