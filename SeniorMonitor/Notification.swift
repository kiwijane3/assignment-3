//
//  Notification.swift
//  SeniorMonitor
//
//  Created by Jane Fraser on 4/10/19.
//  Copyright © 2019 Luoja Labs. All rights reserved.
//

import Foundation

public extension Notification.Name {
	
	public static let clientUpdate = Notification.Name("clientUpdate");

	public static let enteringBackground = Notification.Name("enteringBackground");
	
	public static let enteringForeground = Notification.Name("enteringForeground");
	
}
