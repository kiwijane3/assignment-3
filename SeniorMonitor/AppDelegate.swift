//
//  AppDelegate.swift
//  SeniorMonitor
//
//  Created by Jane Fraser on 1/10/19.
//  Copyright © 2019 Luoja Labs. All rights reserved.
//

import UIKit
import CocoaMQTT
import BackgroundTasks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		UNUserNotificationCenter.current().requestAuthorization(options: [.alert]) { (_, _) in
			
		};
		BGTaskScheduler.shared.register(forTaskWithIdentifier: "fraserjane_mqtt_background_fetch", using: nil) { (task) in
			debugPrint("Initiating background fetch");
			Client.shared.sendNotificationIfNeeded();
			Client.shared.backgroundFetch { (receivedData) in
				task.setTaskCompleted(success: receivedData);
			}
		}
		return true
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		Client.shared.enterBackground();
		// Schedule background fetch
		try? BGTaskScheduler.shared.submit(BGAppRefreshTaskRequest(identifier: "fraserjane_mqtt_background_fetch"));
	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
		Client.shared.enterForeground();
		// Cancel background fetch while we are in the foreground.
		BGTaskScheduler.shared.cancelAllTaskRequests();
	}
	
	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}


}

