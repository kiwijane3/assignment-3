//
//  WarningTimer.swift
//  SeniorMonitor
//
//  Created by Jane Fraser on 2/10/19.
//  Copyright © 2019 Luoja Labs. All rights reserved.
//

import Foundation
import UIKit

public typealias ColourPair = (a: UIColor, b: UIColor);

// These include the given value, but are truncated for to be more concise.
public enum WarningMode {
	case greaterThan
	case lessThan
}

// General functions for progress warning views.
public protocol WarningProgressView {
	
	// The progress the view is displaying.
	var progress: Double {
		get
		set
	}
	
	// These two variables determine whether the view is in warning mode. If warning mode is .greaterThan, then the view is in warning mode if the progress is greater than or equal to threshold. If it is .lessThan, then the view is in warning mode if the progress is less than or equal to the threshold.
	
	var warningMode: WarningMode {
		get
		set
	}
	
	var threshold: Double {
		get
		set
	}
	
}

public extension WarningProgressView {
	
	public var shouldWarn: Bool {
		switch warningMode {
		case .greaterThan:
			return progress >= threshold;
		case .lessThan:
			return progress <= threshold;
		}
	}
	
}

// The warning progress view is a circular progress bar that changes to an ominous colour when the progress is 1.0 or greater. It also has a label.
public class WarningCircularProgressView: UIView, WarningProgressView {
	
	// The progress to be represented. From 0.0 to 1.0; Higher values are displayed as if they were 1.0.
	public var progress: Double = 0 {
		didSet {
			setNeedsDisplay();
		}
	}
	
	// Determines whether the warning should be displayed if the progress is equal to or greater than, or less than or equal to, the threshold.
	public var warningMode: WarningMode = .greaterThan {
		didSet {
			setNeedsDisplay();
		}
	}
	
	// The value used to determine whether there should be a warning. If the progress is greater than, or
	public var threshold: Double = 1.0{
		didSet {
			setNeedsDisplay();
		}
	}
	
	public var text: String = "" {
		didSet {
			label?.text = text;
			setNeedsLayout();
		}
	}
	
	public var okayColours: ColourPair = (a: .systemTeal, b: .systemBlue) {
		didSet {
			setNeedsDisplay();
		}
	}
	
	public var warningColours: ColourPair = (a: .systemPink, b: .systemRed) {
		didSet {
			setNeedsDisplay();
		}
	}
	
	// The width of the line around the background that indicates progress.
	public var progressBarWidth: CGFloat = 16;
	
	// Returns the current colours based on whether we should be warning the user.
	public var currentColours: ColourPair {
		get {
			if shouldWarn {
				return warningColours;
			} else {
				return okayColours;
			}
		}
	}
	
	public var label: UILabel?;
	
	public override init(frame: CGRect) {
		super.init(frame: frame);
		setupLabel();
		setNeedsDisplay();
	}
	
	public required init?(coder: NSCoder) {
		super.init(coder: coder);
		setupLabel();
		setNeedsDisplay();
	}
	
	public init() {
		super.init(frame: .zero);
		setupLabel();
		setNeedsDisplay();
	}
	
	private func setupLabel() {
		let label = UILabel();
		self.addSubview(label);
		label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true;
		label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true;
		self.label = label;
		label.setNeedsDisplay();
		self.label?.translatesAutoresizingMaskIntoConstraints = false;
		self.translatesAutoresizingMaskIntoConstraints = false;
	}
	
	public override func draw(_ rect: CGRect) {
		// Calculate the center of the view in the bounds.
		let center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2);
		// The radius of both circles are reduced by half the progress radius so that the full width of the progress bar can be displayed.
		let radius = (self.frame.width / 2) - progressBarWidth / 2
		// Draw the background circle.
		let background = UIBezierPath(arcCenter: center, radius: radius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: false);
		currentColours.a.setFill();
		background.fill();
		// Draw the progress bar around the background.
		// The top of the circle is defined as 3/2 * pi;
		let progressStart = ( 3 / 2 ) * CGFloat.pi;
		// Progress is from the top of the circle, and difference in the angle is progress * 2pi, since a circle's total angle is 2pi radians. Cap the angle at 2pi radians to avoid bugs.
		let progressEnd = progressStart + min(CGFloat(progress) * (2 * CGFloat.pi), 2 * CGFloat.pi);
		let progressBar = UIBezierPath(arcCenter: center, radius: radius, startAngle: progressStart, endAngle: progressEnd, clockwise: true);
		progressBar.lineWidth = 8;
		progressBar.lineCapStyle = .round;
		currentColours.b.setStroke();
		progressBar.stroke();
	}
	
}

// This view displays a linear progress bar that becom
public class WarningLinearProgressView: UIView, WarningProgressView {
	
	public var progress: Double = 1.0 {
		didSet {
			setNeedsDisplay();
		}
	}
	
	public var warningMode: WarningMode = .greaterThan {
		didSet {
			setNeedsDisplay();
		}
	}
	
	public var threshold: Double = 1.0 {
		didSet {
			setNeedsDisplay();
		}
	}
	
	public var okayColour: UIColor = .systemGreen{
		didSet {
			setNeedsDisplay();
		}
	}
	
	public var warningColour: UIColor = .systemRed {
		didSet {
			setNeedsDisplay();
		}
	}
	
	// The colour we need to display, based on the progress.
	public var currentColour: UIColor {
		get {
			if shouldWarn {
				return warningColour;
			} else {
				return okayColour;
			}
		}
	}
	
	public var lineWidth: CGFloat = 8 {
		didSet {
			setNeedsDisplay();
		}
	}
	
	// The horizontal margin between the view's bounds and the ends of the line. Defaults to 16 for aesthetic reasons and to accommodate that line's rounded edges.
	public var inset: CGFloat = 8 {
		didSet {
			setNeedsDisplay();
		}
	}
	
	public override var intrinsicContentSize: CGSize {
		get {
			// We want this view to have an intrinsic height of the line width so it will be tall enough to display the progress bar, and no intrinsic width, as we want to size our width based on constraints.
			return CGSize(width: UIView.noIntrinsicMetric, height: lineWidth);
		}
	}
	
	
	
	public override init(frame: CGRect) {
		super.init(frame: frame);
		setNeedsDisplay();
	}
	
	public required init?(coder: NSCoder) {
		super.init(coder: coder);
		setNeedsDisplay();
	}
	
	public init() {
		super.init(frame: .zero);
		setNeedsDisplay();
	}
	
	public override func draw(_ rect: CGRect) {
		print(progress);
		print(self.frame);
		// Calculate the center y value in this view's coordinate system.
		let centerY = self.bounds.size.height / 2;
		// The start of the progress line is a certain numbers of points in from view's edge, defined by self.inset, which default for 16. The edge is expected to be lined up to a label, and having a horizontal gap between the label and the start of the line is more aesthetically pleasing. 16 Points give space for this, as well as accommodating the rounded end.
		// The progress line is centered in the view.
		let progressStart = CGPoint(x: inset, y: centerY);
		// When progress is 1.0, the end of the line is inset points horizontally from the right of the view, for the purpose of symmetry. When it is less, the end is a fraction of this length proportional to progress.
		let progressLength = (self.frame.size.width - ( inset * 2.0 )) * CGFloat(min(progress, 1.0));
		let progressEnd = CGPoint(x: inset + progressLength, y: centerY);
		// Initialise the line.
		let progressLine = UIBezierPath();
		progressLine.lineCapStyle = .round;
		progressLine.lineWidth = self.lineWidth;
		// Create the path
		progressLine.move(to: progressStart);
		progressLine.addLine(to: progressEnd);
		// Set the colour.
		self.currentColour.setStroke();
		progressLine.stroke();
	}
	
}
